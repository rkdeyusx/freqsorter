 #include<stdio.h>
 #include<string.h>
/* known bugs - doesn't work with Cyrillic fonts on Linux (yet)
to create a frequency chart, put a text document saved as source.txt in the program folder and "result.txt" for results. 
*/

 struct list
 {
     char*s;
     int num;
     list*next;
     list*prev;
 };
 void linkLIST(list* f, list* l){
     f->next=l;
     l->prev=f;
     f->prev=0;
     l->next=0;
     f->num=99999;
 }
 char sym(char c) {
     unsigned char table[256]= {
         0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 12, 13, 14, 15,//
         16, 17, 18, 19, 0, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, //
         0, 0, 0, 35, 36, 37, 38, 0, 0, 0, 42, 43, 0, 0, 0, 47, //четвёртая справа отвечает за дефис
         48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 0, 60, 61, 62, 0, //63
         64, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, //79
         112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 0, 0, 0, 0, 0, //95
         0, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, //111
         112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 0, 0, 0, 0, 0, //0
         160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, //143
         224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, //159
         160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 0, 172, 173, 174, 175, //175
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //191
         224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, //207
         240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, //223
         224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, //239
         240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
     }; //255
     return table[(unsigned  char)c];
 }
 void code( char str[])
 {
     int k=strlen(str);
     for(int i=0;i<k;i++)//для каждого символа перекодирует регистр
         str[i]=sym(str[i]);
 }
 list*formSTRUCT(char s[])//формирование структуры из массива
 {
     list*node=new list;
     node->s=new char[strlen(s)+1];
     strcpy(node->s,s);
     node->num=1;

     return node;
 }
 void add2LIST(list*beg,list*end,char str[]) { //добавляет структуру в конец списка.//мб переписать во имя лексокографического
     list *p;
     p=formSTRUCT(str);//формирование элемента если нет повторений
     p->next=end;
     p->prev=end->prev;
     end->prev->next=p;
     end->prev=p;
 }
 void swap(list* i, list* j) { //осуществляет перестановку элементов в соответствии с изменившейся частотностью
     i->prev->next=i->next;//предшествовавший i элемент теперь ссылается на следующий
     i->next->prev=i->prev;//следующий за i элемент связан с предшествующим i элементом
     i->next=j;
     i->prev=j->prev;
     j->prev->next=i;
     j->prev=i;
 }
 void putword(list*beg,list*end,char str[]) { //читает список до конца, ищет повторения строки и перемещает старый элемент на новое место между элементами
     list*i=beg->next;
     list*j;
     int n=0;
     //int count=0;
     /*list* frek[50];
     for(int n=0; n<50; n++) {
     frek[n]=NULL;
     }*/
     while(i!=end) { //находит совпадение str и i->s
         if(strcmp(i->s,str)==0) {
             n++;//индикатор того, что нашлось хотя бы одно повторение
             i->num++;


             if(i->num<=i->prev->num) {
                 break;//прерывает цикл, если старший сосед не меньше
             }

             j=i->prev;//создаём индекс-бегунок
             while((j!=beg)&&(i->num >= j->num)) { //находит место для элемента с повторением, и перемещаю его//ограничил цикл, -5 сек

                 if((i->num > j->num)&&(((i->num <= j->prev->num)))) { //ограничил количество перезаписей, тоже минус пять сек
                     swap(i, j);
                 }



                 j=j->prev;//переходим левее по циклу j!=beg, поскольку элементы с одинаковыми частотностями надо пробежать
             }
             break;
         }
         i=i->next;//ПЕРЕХОДИМ К СЛЕДУЮЩЕМУ ЭЛЕМЕНТУ СПИСКА ДЛЯ СРАВНЕНИЯ ЕГО С ПОСТУПИВШИМ ЗНАЧЕНИЕМ
     }//если повторений нет, то в конец
     if(n==0) { //если не нашлось вхождений слова создаём элемент
         add2LIST(beg, end, str);
     }
 }
 void filenotfound(FILE*f){
     if(f==NULL) {
             perror("File not found");
             return;
     }
 }

 void readandform(FILE* f, char str[], list* beg, list* end){
     while(fscanf(f,"%s",str)!=EOF)
     {
         code(str);
         if(strcmp(str,"")==0)
             continue;
         list*p;
         //p=formEL(str);
         putword(beg,end,str);
     }
     return;
 }
 void read(list*beg,list*end) {
     char k[100];
     //int n=0; лишнее
     FILE*f=fopen("source.txt","r");
     filenotfound(f);
     //fscanf(f,"%s",k);
     char str[4096];
     //strcpy(str,k);
     //add2LIST(beg, end, str);
     readandform(f, str, beg, end);

 }

 void prntlst(list*beg,list*end, FILE*f) {//печать списка
     for(beg=beg->next; beg!=end; beg=beg->next)
         fprintf(f,"%s - %d times\n",beg->s,beg->num);
         fclose(f);

         /*setlocale( LC_ALL,"Russian" );
         for(int x=0, beg=beg->next; beg!=end; beg=beg->next){
         fprintf(f,"%s - %d times\n",beg->s,beg->num);
             if(x<100){
             printf("%s - %d times\n", beg->s, beg->num);
             }
             if(x<100) x++;
         }*/

 }
 void pr(list*beg,list*end) { //открывает файл и запускает функцию печати
     FILE*f;
     f=fopen("result.txt","w");
     prntlst(beg,end,f);
 }
 int main() {
     list*f = new list;
     list*l = new list;
     linkLIST(f, l);
     puts("Welcome to frequency sorter. Proceeding to form the chart in result.txt\n\nProcessing the data. It may take up to a few minutes for large files...\n");
     read(f,l);
     puts("Printing results\n");
     pr(f,l);
     puts("Finished.\n\n");
 }
